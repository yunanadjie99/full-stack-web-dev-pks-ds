var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

// soal 1
var daftarHewanSort = daftarHewan.sort();

daftarHewanSort.forEach(function (hewan) {
  console.log(hewan);
});

//soal 2
function introduce() {
  return (
    "Nama saya " +
    data["name"] +
    ", umur saya " +
    data["age"] +
    " tahun" +
    ", alamat saya di " +
    data["address"] +
    ", dan saya punya hobby yaitu " +
    data["hobby"]
  );
}

var data = {
  name: "John",
  age: 30,
  address: "Jalan Pelesiran",
  hobby: "Gaming",
};

var perkenalan = introduce(data);
console.log(perkenalan);

//soal 3
function hitung_huruf_vokal(nama) {
  var splitString = nama.toLowerCase().split("");
  var hurufVokal = [];
  splitString.forEach(function (item) {
    if (
      item == "a" ||
      item == "i" ||
      item == "u" ||
      item == "e" ||
      item == "o"
    ) {
      hurufVokal.push(item);
    }
  });
  var hitungHurufVokal = hurufVokal.length;
  return hitungHurufVokal;
}

var hitung_1 = hitung_huruf_vokal("Muhammad");

var hitung_2 = hitung_huruf_vokal("Iqbal");

console.log(hitung_1, hitung_2);
