// soal 1
//jawaban versi gabung
let p = 8;
let l = 5;
const angka = 2;

const luasKelilingPersegiPanjang = (a, b) => {
  let luas = a * b;
  let keliling = angka * (a + b);
  console.log(luas);
  console.log(keliling);
};

luasKelilingPersegiPanjang(p, l);

// jawaban versi pisah
const luasPersegiPanjang = (a, b) => {
  let luas = a * b;
  console.log(luas);
};
luasPersegiPanjang(p, l);

const kelilingPersegiPanjang = (a, b) => {
  let keliling = angka * (a + b);
  console.log(keliling);
};
kelilingPersegiPanjang(p, l);

console.log("\n");

// soal 2
const newFunction = (firstName, lastName) => {
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: () => {
      //   console.log(firstName + " " + lastName);
      console.log(`${firstName} ${lastName}`);
    },
  };
};

newFunction("William", "Imoh").fullName();

console.log("\n");

// soal 3

const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
};

//jawaban
const { firstName, lastName, address, hobby } = newObject;

console.log(firstName, lastName, address, hobby);

console.log("\n");

// soal 4
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];

//jawaban
let combined = [...west, ...east];
console.log(combined);

// soal 5
const planet = "earth";
const view = "glass";
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`;
console.log(before);
